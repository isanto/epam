window.onload = function() {
    loadUsers();
};

var users;
var clicked = false;

function loadUsers() {
	var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture', true);
	xhr.send();
    xhr.onreadystatechange = function() {
    	if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
        	alert(xhr.status + ': ' + xhr.statusText);
        } else {
        	try {
            	var data = JSON.parse(xhr.responseText); //////////////////
            	var index = 0;
            	users = data.results;
            	users.forEach(function(user) { user.id = index++; });
          	} catch (e) {
            	alert("Некорректный ответ: " + e.message);
          	}
          	try {
				showUsers(users);
			}
          	catch (e) {
				alert(e.message);
			}
        }
    }
}

function showUsers(users) { 
	userList.innerHTML = "";
	
    users.forEach(function(user) {  	
    	var li = userList.appendChild(document.createElement('li'));
        li.innerHTML =/* user.id + ' ' +*/ user.name.title + '. ' + user.name.first + ' ' + user.name.last;
        
     	li.onmouseover = function() {
     		if (!clicked) {
   				showPreview(user);
     			preview.style.visibility = "visible";
     		}
     	}
     	
     	li.onclick = function() {
     		clicked = true;
     		showPreview(user);
     		preview.style.visibility = "visible";
     		closeBtn.style.visibility = "visible";
//     		userList.childNodes[user.id].className = "active";
     	} 
     	
     	li.onmouseleave = function() {
     		if (!clicked) { 
     			preview.style.visibility = "hidden"; 
     			closeBtn.style.visibility = "hidden";
     		}
     	}
		     
        li.appendChild(document.createElement('br'));
        
        var img = li.appendChild(document.createElement('img'));
        img.src = user.picture.medium;
      });		
}