var sortedBy = null;

function sortByFName() { 
	FName.className = "active";
	LName.className = "";
	Letter.className = "";
	if (sortedBy == "FNameAsc") {
		users = users.sort(byFName).reverse();
      	showUsers(users);
		sortedBy = "FNameDesc";
			
		FName.innerHTML = "First Name ▼";
		LName.innerHTML = "Last Name";
		Letter.innerHTML = "First Letter";
	} else {
		users = users.sort(byFName);
      	showUsers(users);
      	sortedBy = 'FNameAsc';
      	FName.innerHTML = "First Name ▲";
      	LName.innerHTML = "Last Name";
      	Letter.innerHTML = "First Letter";
	}	
}

function byFName(a, b) {
	var a = a.name.first + a.name.last;
	var b = b.name.first + b.name.last;
	
	if (a == b)
    	return 0;
	if (a < b)
    	return -1;
	else
   		return 1; 
}

function sortByLName() {
	LName.className = "active";
	FName.className = "";
	Letter.className = "";
	
	if (sortedBy == "LNameAsc") {
		users = users.sort(byLName).reverse();
      	showUsers(users);
		sortedBy = "LNameDesc";
		LName.innerHTML = "Last Name ▼";
		FName.innerHTML = "First Name";
		Letter.innerHTML = "First Letter";
	}
	else {
		users = users.sort(byLName);
      	showUsers(users);
		sortedBy = "LNameAsc";
		LName.innerHTML = "Last Name ▲";
		FName.innerHTML = "First Name";
		Letter.innerHTML = "First Letter";
	}
}

function byLName(a, b) {
	var a = a.name.last + a.name.first;
	var b = b.name.last + b.name.first;
	
	if (a == b)
    	return 0;
	if (a < b)
    	return -1;
	else
   		return 1; 
}

function sortByLetter() {
	Letter.className = "active";
	LName.className = "";
	FName.className = "";
	if (sortedBy == "LetterAsc") {
		users = users.sort(byLetter).reverse();
      	showUsers(users);
		sortedBy = "LetterDesc";
		Letter.innerHTML = "First Letter ▼";
		LName.innerHTML = "Last Name";
		FName.innerHTML = "First Name";
	}
	else {
		users = users.sort(byLetter);
      	showUsers(users);
		sortedBy = "LetterAsc";
		Letter.innerHTML = "First Letter ▲";
		LName.innerHTML = "Last Name";
		FName.innerHTML = "First Name";
	}
}

function byLetter(a, b) {
	if (a.name.first[0] == b.name.first[0])
    	return 0;
	if (a.name.first[0] < b.name.first[0])
    	return -1;
	else
   		return 1; 
}