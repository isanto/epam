function showPreview(user) {     
    userPic.src = user.picture.large;
    userName.innerHTML = user.name.title + '. ' + user.name.first + ' ' + user.name.last;
    
    userStreet.innerHTML = user.location.street;
    userCity.innerHTML = user.location.city;
    userState.innerHTML = user.location.state;
    userEmail.innerHTML = user.email;
    userPhone.innerHTML = user.phone;
}

function hidePreview() {
	preview.style.visibility = "hidden";
	closeBtn.style.visibility = "hidden";
	clicked = false;
} 

function activateCloseBtn() {
	closeBtn.style.color = "#32718b";
} 

function deactivateCloseBtn() {
	closeBtn.style.color = "#9fccdf";
} 